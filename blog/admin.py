from django.contrib import admin
from .models import Post

# Register your models here.


class PostAdmin(admin.ModelAdmin):
    model = Post
    list_display = ('title', 'slug', 'date_creation', 'date_update', 'date_autopublish', 'status', 'author', )
    list_filter = ('title', 'slug', 'date_creation', 'date_update', 'date_autopublish', 'status', 'author', )
    search_fields = ('title', 'slug', 'body', 'category', 'author', )
    actions = ['publicar']

    def publicar(self, request, queryset):
        queryset.update(status='P')
    publicar.short_description = "Marcar post como publicados."


admin.site.register(Post, PostAdmin)
