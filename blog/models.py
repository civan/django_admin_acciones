from django.db import models
from django.contrib.auth.models import User
from django.template.defaultfilters import slugify

# Create your models here.


class Post(models.Model):
    POST_STATUS = (
        ('D', 'DRAFT'),
        ('R', 'READY'),
        ('P', 'PUBLISHED'),
    )

    title = models.CharField(max_length=250)
    slug = models.SlugField(max_length=100, blank=True, null=True)
    date_creation = models.DateTimeField(auto_now_add=True)
    date_update = models.DateTimeField(auto_now=True)
    date_autopublish = models.DateTimeField(blank=True, null=True)
    status = models.CharField(
        max_length=1,
        choices=POST_STATUS,
        default='D',
    )
    body = models.TextField()
    author = models.ForeignKey(User)

    def __str__(self):
        return self.title

    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = slugify(self.title)
        return super(Post, self).save(*args, **kwargs)
